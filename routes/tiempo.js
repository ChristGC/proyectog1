const { Router } = require('express');
const router = Router();
const DB = require('../config/oracle');

router.get('/',(req, res)=>{
    res.status(200).json({
        message: "este mensaje es desde el servidor"
    })
});
router.get('/tiempo', async (req, res)=>{
    const tiempo = [];
    sql="select * from tiempo";

    let result = await DB.Open(sql,[],false);
    
    //console.log(result.rows);
    console.log(tiempo)
    result.rows.map(news=>{
        let newsSchema ={
            "id_tiempo": news[0],
            "tiempo_r": news[1],
            "tiempo_v": news[2],
            "idsemaforo": news[3]
            
        }
        tiempo.push(newsSchema);
    });
    res.json({tiempo});
});
router.put('/tiempo', async (req, res)=>{
    const {id_tiempo, tiempo_r, tiempo_v,idsemaforo}= req.body;
 
    sql= "update tiempo set tiempo_r=:tiempo_r, tiempo_v=:tiempo_v, idsemaforo=:idsemaforo  where id_tiempo=:id_tiempo";
 
    await DB.Open(sql, {id_tiempo, tiempo_r, tiempo_v,idsemaforo}, true);
 
    res.status(200).json({
        "id_tiempo": id_tiempo,
        "tiempo_r": tiempo_r,
        "tiempo_v": tiempo_v,
        "idsemaforo": idsemaforo
    })
 
 });

 module.exports = router;