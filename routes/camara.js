const { Router } = require('express');
const router = Router();
const DB = require('../config/oracle');

router.get('/',(req, res)=>{
    res.status(200).json({
        message: "este mensaje es desde el servidor"
    })
});
router.get('/camara', async (req, res)=>{
    const camara = [];
    sql="select * from camara";

    let result = await DB.Open(sql,[],false);
    
    //console.log(result.rows);
    console.log(camara)
    result.rows.map(news=>{
        let newsSchema ={
            "id_camara": news[0],
            "Estado": news[1]
        }
        camara.push(newsSchema);
    });
    res.json({camara});
});
router.put('/camara', async (req, res)=>{
    const { id_Camara, estado}= req.body;
 
    sql= "update camara set estado=:estado where id_Camara=:id_Camara";
 
    await DB.Open(sql, {id_Camara, estado}, true);
 
    res.status(200).json({
        "id_Camara": id_Camara,
        "estado": estado,
    })
 });
 module.exports = router;