const { Router } = require('express');
const router = Router();
const DB = require('../config/oracle');

router.get('/operador', async (req, res) => {
    const operador = [];
    sql = "select * from operador";

    let result = await DB.Open(sql, [], false);

    console.log(operador)
    result.rows.map(news => {
        let newsSchema = {
            "id_operador": news[0],
            "nombre": news[1],
            "apellido": news[2],
            "cedula": news[3],
            "email": news[4],
            "genero": news[5],
            "edad": news[6]
        }
        operador.push(newsSchema);
    });
    res.json({ operador });
});

module.exports = router;