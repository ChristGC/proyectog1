const { Router } = require('express');
const router = Router();
const DB = require('../config/oracle');

router.get('/',(req, res)=>{
    res.status(200).json({
        message: "este mensaje es desde el servidor"
    })
});
router.get('/semaforo', async (req, res)=>{
    const semaforo = [];
    sql="select * from semaforo";

    let result = await DB.Open(sql,[],false);
    
    //console.log(result.rows);
    console.log(semaforo)
    result.rows.map(news=>{
        let newsSchema ={
            "id_semaforo": news[0],
            "estado": news[1],
            "idcamara": news[2],
            "idsensor": news[3]
            
        }
        semaforo.push(newsSchema);
    });
    res.json({semaforo});
});
router.put('/semaforo', async (req, res)=>{
    const {id_semaforo, estado, idcamara,idsensor}= req.body;
 
    sql= "update Semaforo set estado=:estado, idcamara=:idcamara, idsensor=:idsensor  where id_semaforo=:id_semaforo";
 
    await DB.Open(sql, {id_semaforo, estado, idcamara,idsensor}, true);
 
    res.status(200).json({
        "id_semaforo": id_semaforo,
        "estado": estado,
        "idcamara": idcamara,
        "idsensor": idsensor
    })
 
 });

 module.exports = router;