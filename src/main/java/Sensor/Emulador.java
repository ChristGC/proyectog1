/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Sensor;

import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;


public class Emulador {
    
    private double peso;
    private int cod;
    private final String objeto= "Sensor";

    public Emulador() {
    }

    public void datos() throws Exception{
        cod = (int)(Math.random()*10);
        peso = Math.random()*(700.50-350.50)+350.50;
        Publicar(cod, peso);
    }
    
    private void Publicar(int cod, double peso)throws Exception {
        String uri = System.getenv("CLOUDAMQP_URL");
        if (uri == null) {
            uri = "amqps://edocvcey:pbGCxKNVwcziQMRVSjsWhwdiXkQH_N_X@gull.rmq.cloudamqp.com/edocvcey";
        }
        ConnectionFactory factory = new ConnectionFactory();
        factory.setUri(uri);
        factory.setConnectionTimeout(30000000);
        Connection connection = factory.newConnection();
        Channel channel = connection.createChannel();

        String queue = " ";
        String exchangeName = "";
        String routingKey = "";
        queue = "codSensor";
        routingKey = queue;
        channel.queueDeclare(queue, false, false, false, null);
        String message = String.valueOf(cod);
        channel.basicPublish(exchangeName = "", routingKey, null, message.getBytes());
        System.out.println("[x] Sent '" + message + "'");
        
        queue = "Peso";
        routingKey = queue;
        channel.queueDeclare(queue, false, false, false, null);
        String message1 = String.valueOf(peso);
        channel.basicPublish(exchangeName = "", routingKey, null, message.getBytes());
        System.out.println("[x] Sent '" + message1 + "'");
        connection.close();
    }
}
