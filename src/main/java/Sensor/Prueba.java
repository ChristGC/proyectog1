/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Sensor;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author kriz_
 */
public class Prueba {

    public static void main(String[] args) throws Exception {
        Emulador emula = new Emulador();
        Consumidor consume = new Consumidor();
        Runnable runnable = new Runnable() {
            @Override
            public void run() {
                // Esto se ejecuta en segundo plano una única vez
                while (true) {
                    // Pero usamos un truco y hacemos un ciclo infinito
                    try {
                        // En él, hacemos que el hilo duerma
                        Thread.sleep(10000);
                        // Y después realizamos las operaciones                        
                        emula.datos();
                        consume.consumir();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    } catch (Exception ex) {
                        Logger.getLogger(Prueba.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
            }
        };
        // Creamos un hilo y le pasamos el runnable
        Thread hilo = new Thread(runnable);
        hilo.start();

        // Y aquí podemos hacer cualquier cosa, en el hilo principal del programa
        System.out.println("Yo imprimo en el hilo principal");

    }

}
