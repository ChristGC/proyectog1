package Sensor;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import com.rabbitmq.client.DeliverCallback;

public class Consumidor {

    public Consumidor() {}

    public void consumir() throws Exception {
        String uri = System.getenv("CLOUDAMQP_URL");
        if (uri == null) {
            uri = "amqps://edocvcey:pbGCxKNVwcziQMRVSjsWhwdiXkQH_N_X@gull.rmq.cloudamqp.com/edocvcey";
        }
        ConnectionFactory factory = new ConnectionFactory();
        factory.setUri(uri);
        factory.setConnectionTimeout(30000);
        Connection connection = factory.newConnection();
        Channel channel = connection.createChannel();
        String queue = " ";
        String exchangeName = "";
        String routingKey = "";
        DeliverCallback deliverCallback = (consumerTag, delivery) -> {
            String message = new String(delivery.getBody(), "UTF-8");
            System.out.println(" [x] Received '" + message + "'");
        };
        channel.basicConsume(queue = "codSensor", true, deliverCallback, consumerTag -> {
        });
        
        channel.basicConsume(queue = "Peso", true, deliverCallback, consumerTag -> {
        });

    }
}
